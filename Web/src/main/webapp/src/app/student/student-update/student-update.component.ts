import {Component, Input, OnInit} from '@angular/core';
import {Student} from "../shared/student.model";
import {StudentService} from "../shared/student.service";
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from "@angular/common";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-student-update',
  templateUrl: './student-update.component.html',
  styleUrls: ['./student-update.component.css']
})
export class StudentUpdateComponent implements OnInit {

  @Input() student: Student;

  constructor(private clientService: StudentService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(switchMap((params: Params) => this.clientService.getStudent(+params['id'])))
      .subscribe(student => this.student = student);
  }

  back(): void {
    this.location.back();
  }


  save(name: string, serialNumber: string, group: string) {
    console.log("updating ", this.student, " => ", name)
    if (name.length != 0)
      this.student.name = name;
    if (serialNumber.length != 0)
      this.student.serialNumber = serialNumber;
    if (group.length > 0)
      this.student.sgroup = +group;

    this.clientService.update(this.student)
      .subscribe(_ => this.back());
  }
}
