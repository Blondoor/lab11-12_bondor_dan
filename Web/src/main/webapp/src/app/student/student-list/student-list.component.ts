import {Component, OnInit} from '@angular/core';
import {Student} from "../shared/student.model";
import {StudentService} from "../shared/student.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  students: Student[];
  selectedStudent: Student;
  pageNo: number = 0;
  pages: number[];

  constructor(private studentService: StudentService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getPaged();
  }

  getPaged(){
    return this.studentService.getPage(this.pageNo).subscribe(
      data => {
        this.students = data['content'];
        this.pages = new Array(data['totalPages']+1);
        console.log(this.pages);
        console.log(data)
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }

  updateSelected(student: Student) {
    console.log("selected: ", student);
    this.selectedStudent = student;
  }

  delete() {
    console.log("deleting student", this.selectedStudent);
    this.studentService.delete(this.selectedStudent.id)
      .subscribe(_ => {
        console.log("done");

        this.students = this.students
          .filter(student => student.id !== this.selectedStudent.id);
      })
  }


  update() {
    this.router.navigate(['students/update/', this.selectedStudent.id]);
  }

  setPage(i: number, $event: MouseEvent) {
    event.preventDefault();
    this.pageNo = i;
    this.getPaged();
  }
}
