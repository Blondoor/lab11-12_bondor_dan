export class Student {
    id: number;
    name: string;
    serialNumber: string;
    sgroup: number;
}
