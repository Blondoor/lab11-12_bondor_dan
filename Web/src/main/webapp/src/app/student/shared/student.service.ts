import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Student} from "./student.model";
import {map} from "rxjs/operators";

@Injectable()
export class StudentService {
  private studentsURL = 'http://localhost:8080/api/students';

  constructor(private httpStudent: HttpClient) {
  }

  getALl(): Observable<Student[]> {
    return this.httpStudent
      .get<Array<Student>>(this.studentsURL);
  }

  delete(id: number) {
    const deleteURL = `${this.studentsURL}/delete/${id}`;
    return this.httpStudent.delete(deleteURL);
  }

  getStudent(id: number) {
    return this.getALl()
      .pipe(map(students => students
        .find(student => student.id == id)));
  }

  update(student: Student) {
    const updateURL = `${this.studentsURL}/${student.id}`;
    return this.httpStudent
      .put<Student>(updateURL, student);
  }

  save(student: Student): Observable<Student> {
    console.log("saving student: ", student);

    return this.httpStudent
      .post<Student>(this.studentsURL, student);
  }

  /*filter(firstname: string, secondname: string, job: string,age: string, sortStudent: Student): Observable<Student[]> {
    if (age.length == 0)
      age = '-1';
    if (firstname.length == 0)
      firstname = firstname + ' '
    if (secondname.length == 0)
      secondname = secondname + ' '
    if (job.length == 0)
      job = job + ' '
    const filterURL = `${this.studentsURL}/${firstname}/${secondname}/${job}/${+age}`
    return this.httpStudent
      .post<Array<Student>>(filterURL, sortStudent);
  }*/

  getPage(page: number) {
    const pageURL = `${this.studentsURL}/page?page=${page}`
    return this.httpStudent.get(pageURL);
  }
}
