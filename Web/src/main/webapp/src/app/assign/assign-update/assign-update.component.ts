import {Component, Input, OnInit} from '@angular/core';
import {AssignService} from "../shared/assign.service";
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from "@angular/common";
import {Assign} from "../shared/assign.model";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-assign-update',
  templateUrl: './assign-update.component.html',
  styleUrls: ['./assign-update.component.css']
})
export class AssignUpdateComponent implements OnInit {

  @Input() assign: Assign;

  constructor(private assignService: AssignService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(switchMap((params: Params) => this.assignService.getAssign(+params['id'])))
      .subscribe(assign => this.assign = assign);
  }

  back() {
    this.location.back();
  }

  save(studentID: string, problemID: string) {
    console.log("updating ", this.assign, ' => ', studentID, ' ', problemID);
    if (studentID.length != 0)
      this.assign.studentID = +studentID;
    if (problemID.length != 0)
      this.assign.problemID = +problemID;

    this.assignService.update(this.assign)
      .subscribe(_ => this.back());
    this.back();
  }
}
