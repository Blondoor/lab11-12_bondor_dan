import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.css']
})
export class AssignComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  add() {
    console.log("add assignment button clicked");
    this.router.navigate(['assignments/new']);
  }

  filter(studentID: string, problemID: string) {
    console.log("filter assignments button clicked: studentID:", studentID, 'problemID:', problemID);
    this.router.navigate(['rentals/filter/:studentID/:movieID', {studentID: studentID, problemID: problemID}]);
  }
}
