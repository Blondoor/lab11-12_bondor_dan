import {Component, OnInit} from '@angular/core';
import {AssignService} from "../shared/assign.service";
import {Location} from "@angular/common";
import {Assign} from "../shared/assign.model";

@Component({
  selector: 'app-assign-new',
  templateUrl: './assign-new.component.html',
  styleUrls: ['./assign-new.component.css']
})
export class AssignNewComponent implements OnInit {

  constructor(private assignService: AssignService,
              private location: Location) {
  }

  ngOnInit(): void {
  }

  save(studentID: string, problemID: string) {
    console.log("save assign ", studentID, ' ', problemID, ' ');

    this.assignService.save({id: 0, studentID: +studentID, problemID: +problemID})
      .subscribe(assign => {
        console.log("saved assignment: ", assign);
        this.back();
      });
  }

  back() {
    this.location.back();
  }

}
