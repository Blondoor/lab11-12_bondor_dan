import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Problem} from "./problem.model";
import {map} from "rxjs/operators";

@Injectable()
export class ProblemService {
  private problemsURL = 'http://localhost:8080/api/problems';

  constructor(private httpClient: HttpClient) {
  }

  getALl(): Observable<Problem[]> {
    return this.httpClient
      .get<Array<Problem>>(this.problemsURL);
  }

  delete(id: number) {
    const deleteURL = `${this.problemsURL}/${id}`;
    return this.httpClient.delete(deleteURL);
  }


  getProblem(id: number) {
    return this.getALl()
      .pipe(map(problems => problems
        .find(problem => problem.id == id)));
  }

  update(problem: Problem) {
    const updateURL = `${this.problemsURL}/${problem.id}`;
    return this.httpClient
      .put<Problem>(updateURL, problem);
  }

  save(problem: Problem): Observable<Problem> {
    console.log("saving problem: ", problem);

    return this.httpClient
      .post<Problem>(this.problemsURL, problem);
  }

  /*filter(serialNumber: string, description: string): Observable<Problem[]> {
    if (description.length == 0)
      description = description + ' '
    const filterURL = `${this.moviesURL}/${serialNumber}/${description}`
    return this.httpClient
      .post<Array<Problem>>(filterURL, sortProblem);
  }*/

  getPage(page: number) {
    const pageURL = `${this.problemsURL}/page?page=${page}`
    return this.httpClient.get(pageURL);
  }
}
