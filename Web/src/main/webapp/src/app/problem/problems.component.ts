import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_CONFIGURATION} from "@angular/router";

@Component({
  selector: 'app-problem',
  templateUrl: './problems.component.html',
  styleUrls: ['./problems.component.css']
})
export class ProblemsComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  add() {
    console.log("add problem button clicked");
    this.router.navigate(['problems/new']);
  }

  filter(serialNumber: string, description: string) {
    console.log("filter button clicked: serialNumber:", serialNumber, " description:", description);
    this.router.navigate(['problems/filter/:title/:description/:rating/:price', {
      serialNumber: serialNumber,
      description: description,
    }]);
  }
}
