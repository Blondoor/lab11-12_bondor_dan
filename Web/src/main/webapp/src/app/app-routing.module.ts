import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentsComponent} from "./student/students.component";
import {StudentNewComponent} from "./student/student-new/student-new.component";
import {StudentUpdateComponent} from "./student/student-update/student-update.component";
//import {StudentFilterComponent} from "./student/student-filter/student-filter.component";
import {ProblemsComponent} from "./problem/problems.component";
import {ProblemNewComponent} from "./problem/problem-new/problem-new.component";
import {ProblemsUpdateComponent} from "./problem/problem-update/problem-update.component";
//import {ProblemFilterComponent} from "./problem/problem-filter/problem-filter.component";
import {AssignComponent} from "./assign/assign.component";
import {AssignUpdateComponent} from "./assign/assign-update/assign-update.component";
//import {AssignFilterComponent} from "./assign/assign-filter/assign-filter.component";
import {AssignNewComponent} from "./assign/assign-new/assign-new.component";

const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },

  {path: 'students', component: StudentsComponent},
  {path: 'students/new', component: StudentNewComponent},
  {path: 'students/update/:id', component: StudentUpdateComponent},
  //{path: 'students/filter/:name/:serialNumber/:group', component: StudentFilterComponent},
  {path: 'problems', component: ProblemsComponent},
  {path: 'problems/new', component: ProblemNewComponent},
  {path: 'problems/update/:id', component: ProblemsUpdateComponent},
  //{path: 'problems/filter/:serialNumber/:description', component: ProblemFilterComponent},
  {path: 'assignments', component: AssignComponent},
  {path: 'assignments/new', component: AssignNewComponent},
  {path: 'assignments/update/:id', component: AssignUpdateComponent},
  //{path: 'rentals/filter/:clientID/:movieID', component: RentalsFilterComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
