import {Component, OnInit} from '@angular/core';
import {Assign} from "../shared/assign.model";
import {AssignService} from "../shared/assign.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-assign-list',
  templateUrl: './assign-list.component.html',
  styleUrls: ['./assign-list.component.css']
})
export class AssignsListComponent implements OnInit {
  assigns: Assign[];
  selectedAssign: Assign;

  constructor(private assignService: AssignService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.assignService.getAll()
      .subscribe(assigns => this.assigns = assigns);
  }

  updateSelected(assign: Assign) {
    this.selectedAssign = assign;
  }

  delete() {
    console.log("deleting assignment ", this.selectedAssign);
    this.assignService.delete(this.selectedAssign.id)
      .subscribe(_ => {
        console.log("done");

        this.assigns = this.assigns
          .filter(assign => assign.id !== this.selectedAssign.id);
      })
  }

  update() {
    this.router.navigate(['assignments/update/', this.selectedAssign.id]);
  }
}
