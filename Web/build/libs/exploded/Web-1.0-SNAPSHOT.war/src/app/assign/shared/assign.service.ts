import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Assign} from "./assign.model";
import {map} from "rxjs/operators";

@Injectable()
export class AssignService {
  private assignsURL = 'http://localhost:8080/api/assignments';

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Assign[]> {
    return this.httpClient
      .get<Array<Assign>>(this.assignsURL);
  }

  save(assign: Assign): Observable<Assign> {
    console.log("saving assignment: ", assign)

    return this.httpClient
      .post<Assign>(this.assignsURL, assign)
  }

  delete(id: number) {
    const deleteURL = `${this.assignsURL}/${id}`;
    return this.httpClient.delete(deleteURL);
  }

  getAssign(id: number) {
    return this.getAll()
      .pipe(map(assigns => assigns
        .find(assign => assign.id == id)));
  }

  update(assign: Assign) {
    const updateURL = `${this.assignsURL}/${assign.id}`;
    return this.httpClient
      .put<Assign>(updateURL, assign);
  }

  filter(studentID: string, problemID: string): Observable<Assign[]> {
    if (studentID.length == 0)
      studentID = '-1';
    if (problemID.length == 0)
      problemID = '-1'
    const filterURL = `${this.assignsURL}/${+studentID}/${+problemID}`
    return this.httpClient
      .get<Array<Assign>>(filterURL);
  }
}
