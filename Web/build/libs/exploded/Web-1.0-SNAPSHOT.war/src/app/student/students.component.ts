import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_CONFIGURATION} from "@angular/router";
//import {ClientsFilterComponent} from "./student-filter/student-filter.component";

@Component({
  selector: 'app-student',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  add() {
    console.log("add student button clicked");
    this.router.navigate(['students/new']);
  }

  filter(name: string, serialNumber: string, group:string) {
    console.log("filter button clicked: name:", name, " serialNumber:", serialNumber, " group:", group);
    this.router.navigate(['students/filter/:name/:serialNumber/:group', {
      name: name,
      serialNumber: serialNumber,
      group: group
    }]);
  }
}
