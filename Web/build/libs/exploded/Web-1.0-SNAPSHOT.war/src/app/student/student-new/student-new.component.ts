import {Component, OnInit} from '@angular/core';
import {Location} from "@angular/common";
import {StudentService} from "../shared/student.service";

@Component({
  selector: 'app-student-new',
  templateUrl: './student-new.component.html',
  styleUrls: ['./student-new.component.css']
})
export class StudentNewComponent implements OnInit {

  constructor(private studentService: StudentService,
              private location: Location) {
  }

  ngOnInit(): void {
  }

  save(name: string, serialNumber: string, group: string) {
    console.log("save client ", name, ' ', serialNumber, ' ', group);

    this.studentService.save({id: 0, name:name, serialNumber:serialNumber, sgroup:+group})
      .subscribe(student => {
        console.log("saved student: ", student);
        this.back();
      });
  }

  back() {
    this.location.back();
  }
}
