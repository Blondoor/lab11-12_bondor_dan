import {Component, OnInit} from '@angular/core';
import {ProblemService} from "../shared/problem.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-problem-new',
  templateUrl: './problem-new.component.html',
  styleUrls: ['./problem-new.component.css']
})
export class ProblemNewComponent implements OnInit {

  constructor(private problemService: ProblemService,
              private location: Location) {
  }

  ngOnInit(): void {
  }

  save(serialNumber: string, description: string) {
    console.log("save problem ", serialNumber, ' ', description);
    serialNumber = serialNumber + ' '
    description = description + ' '
    this.problemService.save({id: 0, serialNumber: serialNumber, descr: description})
      .subscribe(problem => {
        console.log("saved problem: ", problem);
        this.back();
      });
  }

  back() {
    this.location.back();
  }
}
