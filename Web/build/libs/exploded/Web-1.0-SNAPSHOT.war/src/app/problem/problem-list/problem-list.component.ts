import {Component, OnInit} from '@angular/core';
import {Problem} from "../shared/problem.model";
import {ProblemService} from "../shared/problem.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-problem-list',
  templateUrl: './problem-list.component.html',
  styleUrls: ['./problem-list.component.css']
})
export class ProblemsListComponent implements OnInit {
  problems: Problem[];
  selectedProblem: Problem;
  pageNo: number = 0;
  pages: number[];

  constructor(private movieService: ProblemService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getPaged();
  }

  getPaged(){
    return this.movieService.getPage(this.pageNo).subscribe(
      data => {
        this.problems = data['content'];
        this.pages = new Array(data['totalPages']+1);
        console.log(this.pages);
        console.log(data)
      },
      (error) => {
        console.log(error.error.message);
      }
    );
  }

  updateSelected(movie: Problem) {
    console.log("selected: ", movie);
    this.selectedProblem = movie;
  }

  delete() {
    console.log("deleting problem", this.selectedProblem);
    this.movieService.delete(this.selectedProblem.id)
      .subscribe(_ => {
        console.log("done");

        this.problems = this.problems
          .filter(problem => problem.id !== this.selectedProblem.id);
      })
  }

  update() {
    this.router.navigate(['problems/update/', this.selectedProblem.id]);
  }

  setPage(i: number, $event: MouseEvent) {
    event.preventDefault();
    this.pageNo = i;
    this.getPaged();
  }
}
