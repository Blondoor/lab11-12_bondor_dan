import {Component, Input, OnInit} from '@angular/core';
import {Problem} from "../shared/problem.model";
import {ProblemService} from "../shared/problem.service";
import {ActivatedRoute, Params} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {Location} from "@angular/common";

@Component({
  selector: 'app-problem-update',
  templateUrl: './problem-update.component.html',
  styleUrls: ['./problem-update.component.css']
})
export class ProblemsUpdateComponent implements OnInit {

  @Input() problem: Problem;

  constructor(private problemService: ProblemService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(switchMap((params: Params) => this.problemService.getProblem(+params['id'])))
      .subscribe(problem => this.problem = problem);
  }

  back() {
    this.location.back();
  }

  save(serialNumber: string, description: string) {
    console.log("updating ", this.problem, " => ", serialNumber, ' ', description);
    if (serialNumber.length != 0)
      this.problem.serialNumber = serialNumber;
    if (description.length != 0)
      this.problem.descr = description;

    this.problemService.update(this.problem)
      .subscribe(_ => this.back());
  }
}
