import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {StudentsComponent} from './student/students.component';
import {StudentListComponent} from './student/student-list/student-list.component';
import {StudentNewComponent} from './student/student-new/student-new.component';
import {StudentUpdateComponent} from './student/student-update/student-update.component';
//import {StudentFilterComponent} from './student/student-filter/student-filter.component';
import {NgxPaginationModule} from "ngx-pagination";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {StudentService} from "./student/shared/student.service";
import {ProblemsComponent} from "./problem/problems.component";
import {ProblemsListComponent} from "./problem/problem-list/problem-list.component";
import {ProblemsUpdateComponent} from "./problem/problem-update/problem-update.component";
import {ProblemNewComponent} from "./problem/problem-new/problem-new.component";
//import {ProblemFilterComponent} from "./problem/problem-filter/problem-filter.component";
import {ProblemService} from "./problem/shared/problem.service";
import {AssignComponent} from "./assign/assign.component";
import {AssignsListComponent} from "./assign/assign-list/assign-list.component";
import {AssignUpdateComponent} from "./assign/assign-update/assign-update.component";
import {AssignNewComponent} from "./assign/assign-new/assign-new.component";
//import {AssignFilterComponent} from "./assign/assign-filter/assign-filter.component";
import {AssignService} from "./assign/shared/assign.service";

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentListComponent,
    ProblemsComponent,
    ProblemsListComponent,
    AssignComponent,
    AssignsListComponent,
    StudentNewComponent,
    StudentUpdateComponent,
    ProblemsUpdateComponent,
    ProblemNewComponent,
    AssignNewComponent,
    AssignUpdateComponent,
    /*StudentFilterComponent,
    ProblemFilterComponent,
    AssignFilterComponent,*/
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule,
    MatTableModule,
    MatSortModule,
    BrowserAnimationsModule
  ],
  providers: [StudentsComponent, StudentService, ProblemsComponent, ProblemService,  AssignComponent, AssignService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
